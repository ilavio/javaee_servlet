<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>check list</title>
<style>
.redtext {
	color: red;
}
</style>
</head>
<body>

	<p>
		Список:
		<c:out value="${listUser}" />
	</p>
	<div>
		<c:if test="${listUser == null}">
			<p class="redtext">Вы ввели неправильный пароль! Вернитесь и
				попробуйте ещё.</p>
		</c:if>
		<form action="http://localhost:8080/servelet-jsp-dpd/" method="get">
			<button>На главную</button>
		</form>
	</div>

</body>
</html>