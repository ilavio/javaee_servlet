package com.il.tlt.data;

/**
 * 
 * @author Илья
 *
 */
public class Secyuriti {
    private final String PASS = "pass";
    private final LIstUsers list = new LIstUsers();

    public String getPASS() {
        return PASS;
    }
    
    public boolean check(String pass) {
        return pass.equals(PASS);
    }
    
    public String getListUsers(boolean check) {
        String listStr = null;
        
        if(check) {
          listStr =  list.toString();
        }
        
        return listStr;
    }
    
}
