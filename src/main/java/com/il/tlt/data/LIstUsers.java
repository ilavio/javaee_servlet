package com.il.tlt.data;

import java.util.ArrayList;
import java.util.List;

import com.il.tlt.model.entity.User;

class LIstUsers {
    private List<User> listUser;

    public LIstUsers() {
        this.listUser = generateUsers();
    }

    private List<User> generateUsers() {
        List<User> listUser = new ArrayList<>();
        listUser.add(new User("Поля", "24", "миллер"));
        listUser.add(new User("Паша", "20", "диллер"));
        listUser.add(new User("Дима", "30", "киллер"));
        return listUser;
    }

    public List<User> getListUser() {
        return listUser;
    }

    public void setListUser(List<User> listUser) {
        this.listUser = listUser;
    }

    @Override
    public String toString() {
        return "LIstUsers [список пользавателей = " + listUser + "]";
    }
    
    
    
}
