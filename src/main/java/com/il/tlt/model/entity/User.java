package com.il.tlt.model.entity;

import java.util.Objects;

/**
 * 
 * @author Илья
 *
 */
public class User {
    private String name;
    private String age;
    private String position;
    
    public User(String name, String age, String position) {
        super();
        this.name = name;
        this.age = age;
        this.position = position;
    }
    
    public User() {    
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAge() {
        return age;
    }
    public void setAge(String age) {
        this.age = age;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(age, name, position);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        return Objects.equals(age, other.age) && Objects.equals(name, other.name)
                && Objects.equals(position, other.position);
    }
    
    @Override
    public String toString() {
        return "User [name=" + name + ", age=" + age + ", position=" + position + "]";
    }
    
}
