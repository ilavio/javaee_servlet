package com.il.tlt;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.il.tlt.data.Secyuriti;

/**
 * 
 * Servlet implementation class LoginServer
 * 
 * @author Iliya
 */
@WebServlet("/")
public class LoginServer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String TYPE = "text/html";
	private final String ENCODING = "UTF-8";
	private Secyuriti secyuriti = new Secyuriti();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServer() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    /**
     * method setting encoding for response
     * @param response
     * @return PrintWriter
     * @throws IOException
     */
    private PrintWriter setDefaultEncoding(HttpServletResponse response) throws IOException {
        response.setContentType(TYPE);
        response.setCharacterEncoding(ENCODING);
        return response.getWriter();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    request.getRequestDispatcher("/views/loginJSP.jsp").forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    String pass = request.getParameter("password");
	    
	    request.setAttribute("listUser", getListUsers(pass));
	    getServletContext().getRequestDispatcher("/views/check.jsp").forward(request, response);
	   
	}
	
	private String getListUsers(String pass) {
	    boolean x = secyuriti.check(pass);
        System.out.println("Данные из JSP получены: " + pass + " проверка - " + x);
        System.out.println("!: " + secyuriti.getListUsers(x));
        
        return secyuriti.getListUsers(x);
	}

}
